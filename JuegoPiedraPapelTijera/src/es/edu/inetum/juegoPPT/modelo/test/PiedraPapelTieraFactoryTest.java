package es.edu.inetum.juegoPPT.modelo.test;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.edu.inetum.juegoPPT.modelo.Lagarto;
import es.edu.inetum.juegoPPT.modelo.Papel;
import es.edu.inetum.juegoPPT.modelo.Piedra;
import es.edu.inetum.juegoPPT.modelo.PiedraPapelTieraFactory;
import es.edu.inetum.juegoPPT.modelo.Spock;
import es.edu.inetum.juegoPPT.modelo.Tijera;

public class PiedraPapelTieraFactoryTest {
	//1- lote pruebas
	PiedraPapelTieraFactory piedra, papel,tijera, lagarto, spock;

	@Before
	public	void setUp() throws Exception {
		//se ejecuta antes de cada prueba
		piedra 	= new Piedra()	;
		papel 	= new Papel()	;
		tijera 	= new Tijera()	;
		//dos nuevos
		lagarto = new Lagarto()	;
		spock 	= new Spock()	;
	}

	@After
	public void tearDown() throws Exception {
		//se ejecuta despues de cada prueba
		piedra = null;
		papel  = null;
		tijera = null;
		//
		lagarto = null;
		spock 	= null;
	}

	@Test
	public void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTieraFactory.getInstance(PiedraPapelTieraFactory.PIEDRA)
													  .getNombre()
													  .toLowerCase());
		
	}
	@Test
	public void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTieraFactory.getInstance(PiedraPapelTieraFactory.PAPEL)
													  .getNombre()
													  .toLowerCase());		
	}

	@Test
	public void testGetInstanceTiera() {
		assertEquals("tijera", PiedraPapelTieraFactory.getInstance(PiedraPapelTieraFactory.TIJERA)
													  .getNombre()
													  .toLowerCase());		
	}
	
	@Test
	public void testGetInstanceLagarto() {
		assertEquals("lagarto", PiedraPapelTieraFactory.getInstance(PiedraPapelTieraFactory.LAGARTO)
													  .getNombre()
													  .toLowerCase());		
	}
	@Test
	public void testGetInstanceSpock() {
		assertEquals("spock", PiedraPapelTieraFactory.getInstance(PiedraPapelTieraFactory.SPOCK)
													  .getNombre()
													  .toLowerCase());		
	}
	
	 //Casos de PIEDRA
	
	@Test
	public void testCompararPiedraGanaATijera() {
		//TODO para mis queridos alumnos testCompararPiedraGanaATijera
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra le gana a tijera", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararPiedraGanaALagarto() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra le gana a lagarto", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararPiedraPierdeConPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra pierdi� con papel", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararPiedraPierdeConSpock() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra pierdi� con spock", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	public void testCompararPiedraEmpataConPiedra() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empata con piedra", piedra.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	//Casos de PAPEL
	@Test
	public void testCompararPapelGanaConPiedra() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel le gano a piedra", papel.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testPapelGanaSpock() {
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel le gano a spock", papel.getDescripcionREsultado()
				  							 	.toLowerCase());
	}
	@Test
	public void testCompararPapelPierdeConTijera() {
		//TODO para mis queridos alumnos testCompararPapelPierdeConTijera
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel perdi� con tijera", papel.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testPapelPierdeConLagarto() {
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel perdi� con lagarto", papel.getDescripcionREsultado()
				  							 	.toLowerCase());
	}
	@Test
	public void testCompararPapelEmpataConPapel() {
		//TODO para mis queridos alumnos testCompararPapelEmpataConPapel
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empata con papel", papel.getDescripcionREsultado()
													  .toLowerCase());
	}
	

	//Casos de TIJERA
	@Test
	public void testCompararTijeraGanaAPapel() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera le gana a papel", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararTijeraGanaALagarto() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera le gana a lagarto", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	
	@Test
	public void testCompararTijeraPierdeConPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera perdi� con piedra", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararTijeraPierdeConSpock() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera perdi� con spock", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararTijeraEmpataConTijera() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empata con tijera", tijera.getDescripcionREsultado()
													  .toLowerCase());
	}	
	
	//Casos de LAGARTO
	@Test
	public void testCompararLagartoGanaASpock() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto le gana a spock", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararLagartoGanaAPapel() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto le gana a papel", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararLagartoPierdeConTijera() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto perdi� con tijera", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararLagartoPierdeConPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto perdi� con piedra", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararLagartoEmpataConLagarto() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empata con lagarto", lagarto.getDescripcionREsultado()
													  .toLowerCase());
	}	
	
	//Casos de SPOCK
	@Test
	public void testCompararSpockGanaATijera() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock le gana a tijera", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararSpockGanaATPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock le gana a piedra", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararSpockPierdeConPapel() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, spock.comparar(papel));
		assertEquals("spock perdi� con papel", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararSpockPierdeConLagarto() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock perdi� con lagarto", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	public void testCompararSpockEmpataConSpock() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empata con spock", spock.getDescripcionREsultado()
													  .toLowerCase());
	}
	
}
